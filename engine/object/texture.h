#ifndef TEXTURE
#define TEXTURE


#include "../../lib/stb_image.h"
#include "../../utils/math_utils.h"
#include <GL/glew.h>
#include <memory.h>
#include <map>
#include <string>

#define BITS_PER_PIXEL 3

class Texture {
public:
    Texture();
    /* Generates a Texture object from a given texture path */
    Texture(const char *texturePath);
    void bind();
    void unbind();
    ~Texture();
protected:
private:
    GLuint bufferID;
    int textureWidth;
    int textureHeight;
    int bitsPerPixel;
    stbi_uc *textureBuffer;
};

#endif